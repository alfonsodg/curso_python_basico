def saludar(lang):
    def saludar_es(param,pais):
        print "Hola",param
        print "Vives en",pais
    def saludar_en(param,country):
        print "Hi",param
        print "You live in",country
    def saludar_de(param,lande):
        print "Hallo",param
        print "Du wohnst auf",lande
    lang_func = {"es": saludar_es,
                 "en": saludar_en,
                 "de": saludar_de}
    return lang_func[lang]


print 'Menu'
idioma=raw_input('Sprache? Language? Lenguaje? (es,en,de):')
nombre=raw_input('Namme? Name? Nombre?:')
pais=raw_input('Lande? Country? Pais?:')
saludar(idioma)(nombre,pais)
