def saludar(lang):  # en
    def saludar_es(param):
        print "Hola",param
    def saludar_en(param):
        print "Hi",param
    def saludar_fr(param):
        print "Salut",param
    lang_func = {"es": saludar_es,
                 "en": saludar_en,
                 "fr": saludar_fr}
    return lang_func[lang]
f = saludar("en")
f('Alfonso')
saludar('es')('Marco')
#saludar.upper().center(10).width()
