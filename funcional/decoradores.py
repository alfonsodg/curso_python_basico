#decoradores: funcion que recibe una funcion como param y
#regresa otra funcion como res


def mi_decorador(funcion):
    def nueva(*args):
        print 'Esta autorizado para ejecutar la funcion', funcion.__name__
#        print "Llamada a la funcion", funcion.__name__
        #print dir(funcion), funcion.__globals__
        funcion(*args)
#        return retorno
    print 'Esta dentro de la funcion mi_decorador',__name__
    return nueva

@mi_decorador
def impresion(param):
    print 'que tal',param

impresion('hola')
#mi_decorador(imp)('Hola')
