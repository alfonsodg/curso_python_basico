palabra = raw_input('Palabra:')
voca = [letra for letra in palabra if letra in
        ['a', 'e', 'i', 'o', 'u']]
cons = [letra for letra in palabra if letra not in
        ['a', 'e', 'i', 'o', 'u']]
print 'Palabra:', palabra, 'Dimension:', len(palabra)
print 'Vocales:', voca, 'Cnt Vocales:', len(voca)
print 'Consonantes', cons, 'Cnt Consonantes', len(cons)
