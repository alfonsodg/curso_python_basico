#filter:filtra los elementos de una lista, regresando otra lista

def es_par(n):
    return (n % 2.0 == 0)

#l = [1, 2, 3,4,6,9]
l = range(1, 1000)
l2 = filter(es_par, l)
print len(l2)
