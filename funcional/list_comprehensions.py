#list-comprehensions:listas para obtener listas

l = [1, 2, 3]

#Lo que hicimos con map
print [n ** 2 for n in l] #if n>1]


#Lo que hicimos con filter/lambda
l2 = [n for n in l if n % 2.0 == 0]
print l2

#Ejemplo mas complejo
l = [0, 1, 2, 3]
m = ["a", "b"]
n = [s * v for s in m
           for v in l
           if v > 0]
print n
