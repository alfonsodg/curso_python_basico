#map: aplica una funcion a cada elemento de una lista

def cuadrado(n):
    return n ** 2

l = [1, 2, 3]
l2 = map(cuadrado, l)
print tuple(l2)
