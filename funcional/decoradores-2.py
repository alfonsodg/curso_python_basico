#mas decoradores

AUTHENTICATED = True

def avisar(funcion_1):
    def interna1(*args, **kwargs):
        funcion_1(*args, **kwargs)
        print "Se ha ejecutado %s" % funcion_1.__name__
    return interna1

def autenticado(funcion_2):
    def interna2(*args, **kwargs):
        if AUTHENTICATED:
            print 'Demostracion',funcion_2.__name__
            funcion_2(*args, **kwargs)
            print 'La llamada es',funcion_2.__name__
        else:
           raise Exception
    return interna2

@autenticado
@avisar
def abrir_puerta():
    print "Abrir puerta"

#@autenticado
#@avisar
#def cerrar_puerta():
#    print "Cerrar puerta"

abrir_puerta()
#cerrar_puerta()
