# file: inquiry.py
# auth: Albert Huang <albert@csail.mit.edu>
# desc: performs a simple device inquiry followed by a remote name request of
#       each discovered device
# $Id: inquiry.py,v 1.7 2006/05/05 19:07:48 albert Exp $
#

import bluetooth


cercanos = bluetooth.discover_devices(lookup_names = True)

print "Encontre %d dispositivos" % len(cercanos)

for name, addr in cercanos:
    print "  %s - %s" % (addr, name)
