# -*- coding:utf-8 -*-


import urllib, urllib2
import re

#Constantes
#Expresiones regulares
BODY_EXP = r"""<body.*?>(?P<contents>.*)</body>"""
TAG_EXP = r"""<.*?>"""
URL = 'http://paginasblancas.com.pe/resultados.asp?'

#-- Conexion con URL --
#Parametros del URL
params = urllib.urlencode({'ap':'de la borda'})
opener = urllib2.build_opener()
#Lectura del URL dentro de una variable/objeto
contenido = opener.open(URL, params).read()
#-- Metodos de ER para extraer contenidos --
#Empleando el metodo search, se nos devuelve un grupo con las coincidencias
strip_1 = re.search(BODY_EXP, contenido,  re.IGNORECASE| re.DOTALL).group(0)
#Empleando el metodo compile, quitamos los tags restantes
strip_1 = re.compile(TAG_EXP).sub('',strip_1)
#Escribo el resultado en un archivo, pero lo mejor es simplificarlo aún más
#e incluso grabar los registros extraidos en un archivo csv
open('texto_extraido.txt','w').write(strip_1)
