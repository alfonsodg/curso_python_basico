# -*- coding: utf-8 -*-


try:
    # for Python 2.x
    import Tkinter as tk
except ImportError:
    # for Python3
    import tkinter as tk
import tkMessageBox
import sqlite3 as sqlite
import os
import sys

if sys.platform=='linux2':
    path=''
    compr1=''
    compr2='> /dev/lp0'
else:
    path=''
    compr1='copy'
    compr2='lpt1'




def show_rows(data):
#    id=get_id()
    print data
    label_2['text'] = data[-1]
    for val in data:
        tmp=tk.Label(root)
        tmp.pack()
        tmp['text']=str(val)
    option(data)
    
def get_id():
    id = id_entry.get()
    if id:
        return id
    else:
        return False

def warning(data):
    tkMessageBox.showwarning(
            u"Informacion",
            data
        )

def printing(data=''):
    status=os.system(compr1+str(data)+compr2)
    if status:
        warning(u"Problemas en la impresion")


def create_data(data):
    valx={'<--&AAAA&-->':data[0],'<--&BBBB&-->':data[1],'<--&CCCC&-->':data[2]}
    ntexto=[]
    texto=open('plantilla').readlines()
    for linea in texto:
        for val in valx:
            ntexto.append(linea.replace(val,str(valx[val])))
    file=open('data','w')
    for linea in ntexto:
        file.write(linea)
    file.close()


def option(data):
    create_data(data)
    if tkMessageBox.askyesno(u"Imprimir", u"Seguro de imprimir?"):
        printing()

    
def query():
    id=get_id()
    if id:
#        query="select * from ticket where id="+str(id)
        sql="select direccionTicket, comisionVentaTicket, montoTotalTicket, imprimioTicket, hashTicket, codigoOperacionTicket, fVentaTicket, fEntregaTicket, numeroAsiento, nMedioEnvio, nCampana, numeroTarjetaTicket, nTipoTarjeta, nMedioPago, nDistrito, nUsuario, aPaternoUsuario, aMaternoUsuario, nPuntoVenta from ticket inner join asiento on ticket.idAsiento=asiento.id inner join medioEnvio on ticket.idMedioEnvio=medioEnvio.id inner join campana on ticket.idCampana=campana.id inner join tipoTarjeta on ticket.idTipoTarjeta=tipoTarjeta.id inner join medioPago on ticket.idMedioPago=medioPago.id inner join distrito on ticket.idDistrito=distrito.id inner join usuario on ticket.idCliente=usuario.id inner join puntoVenta on ticket.idPuntoVenta=puntoVenta.id where ticket.id=%s"%(id)
#        print sql
        cursor.execute(sql)
        res=cursor.fetchone()
#        print res
        if res:
            show_rows(res)
        else:
            warning(u"Ticket no disponible")

#DB connection
try:
    conex=sqlite.connect(path+"db.db")
except:
    warning(u"Problemas con la BD")
cursor=conex.cursor()


root = tk.Tk()
root.title(u"MacTicket - Modulo de Impresion")
root.geometry('400x400+200+200')

label_1 = tk.Label(root, text=u"Ingresar ID del Ticket: ")
# shows only * when entering a password
id_entry = tk.Entry(root)#, show="*")
# cursor here
id_entry.focus_set()
# click on button to query the db
button = tk.Button(root, text=u"Consultar", command=query)
# label to show result
label_2 = tk.Label(root)

# simple widget layout, stack vertical
label_1.pack(pady=2)
id_entry.pack(pady=5)
button.pack(pady=5)
label_2.pack(pady=5)

# start the event loop
root.mainloop()
