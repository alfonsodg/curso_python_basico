#!/usr/bin/env python
#
# A simple SPIDER image viewer.
# Image (Python Imaging Library) used to convert SPIDER image,
# Tkinter used for display.

from Tkinter import *
import Image, ImageTk
import sys

#im=''

def go():
    print filename
#    global im
#    im=im.resize((200,200),resample=0)


if not sys.argv[1:]:
    print "Usage: python simpleview.py filename"
    sys.exit()
filename = sys.argv[1]

# open the file with the Python Imaging library (PIL)
im = Image.open(filename)
im = im.resize((400,400),resample=0)


root = Tk()  # initialize Tkinter
# convert the PIL image into a Tk-compatible image
tkimage = ImageTk.PhotoImage(im)
root.title('Visualizador de Imagenes')
# Paste the Tk image into a Tkinter Label
Label(root, image=tkimage).grid(row=0,column=0)#pack
Button(root, text='Buscar', command=go).grid(row=0,column=1)

root.mainloop()  # start the GUI
