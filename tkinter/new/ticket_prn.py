
# -*- coding: utf-8 -*-


try:
    # for Python 2.x
    import Tkinter as tk
except ImportError:
    # for Python3
    import tkinter as tk
import tkMessageBox
import sqlite3 as sqlite
import os
import sys

if sys.platform=='linux2':
    path=''
    compr1=''
    compr2='> /dev/lp0'
else:
    path='F:\\serv13\\'
    compr1='copy'
    compr2='lpt1'

def apply_template(data):
    campos_dict={'pventa':data[-1],'apaterno':data[-3],'amaterno':data[-2],'nombre':data[-4]}
    '''
    f = open('data_template','r')
    fw = open('data_print','w')
    data_list=f.readlines()
    for patron,valor in campos_dict.iteritems():
    data_list = [line.replace(patron,valor) for line in data_list]
    f.close()
    fw.writelines(data_list)
    fw.close
    '''
    data_list=[linea.replace(patron,valor) for linea in open('data_template').readlines() for patron,valor in campos_dict.iteritems()]
    open('data_print','w').writelines(data_list)

def show_rows(data):
#    id=get_id()
    print data
    apply_template(data)
    label_2['text'] = data[-1]
    #label_2.grid(row=0,column=1)
    nrow=6
    ncolumn=1
    nombre=data[-4]+" "+data[-2]+" "+data[-3]
    nombre_label=tk.Label(root)
    nombre_valor=tk.Label(root)
    nombre_label['text']="Nombre"
    nombre_valor['text']=nombre
    nombre_label.grid(row=7,column=1)
    nombre_valor.grid(row=7,column=2)
    pv_label=tk.Label(root)
    pv_valor=tk.Label(root)
    pv_label['text']="Punto de Venta"
    pv_valor['text']=data[-1]
    pv_label.grid(row=6,column=1)
    pv_valor.grid(row=6,column=2)
    #for val in data:
	#tmp=tk.Label(root)
	#tmp.grid(row=nrow,column=ncolumn)
	#tmp['text']=str(val)
	#ncolumn = ncolumn+1
    option()
    
def get_id():
    id = id_entry.get()
    if len(id)>0:
        return id
    else:
        return False

def warning(data):
    tkMessageBox.showwarning(
            u"Informacion",
            data
        )

def printing(data=''):
    status=os.system(compr1+str(data)+compr2)
    if status:
        warning(u"Problemas en la impresion")


def option():
    if tkMessageBox.askyesno(u"Imprimir", u"Seguro de imprimir?"):
        printing()

    
def query():
    id=get_id()
    if id:
#        query="select * from ticket where id="+str(id)
        sql="select direccionTicket, comisionVentaTicket, montoTotalTicket, imprimioTicket, hashTicket, codigoOperacionTicket, fVentaTicket, fEntregaTicket, numeroAsiento, nMedioEnvio, nCampana, numeroTarjetaTicket, nTipoTarjeta, nMedioPago, nDistrito, nUsuario, aPaternoUsuario, aMaternoUsuario, nPuntoVenta from ticket inner join asiento on ticket.idAsiento=asiento.id inner join medioEnvio on ticket.idMedioEnvio=medioEnvio.id inner join campana on ticket.idCampana=campana.id inner join tipoTarjeta on ticket.idTipoTarjeta=tipoTarjeta.id inner join medioPago on ticket.idMedioPago=medioPago.id inner join distrito on ticket.idDistrito=distrito.id inner join usuario on ticket.idCliente=usuario.id inner join puntoVenta on ticket.idPuntoVenta=puntoVenta.id where ticket.id=%s"%(id)
#        print sql
        cursor.execute(sql)
        res=cursor.fetchone()
#        print res
        if res:
            show_rows(res)
        else:
            warning(u"Ticket no disponible")

#DB connection
try:
    conex=sqlite.connect(path+"db.db")
except:
    warning(u"Problemas con la BD")
cursor=conex.cursor()




root = tk.Tk()
root.title(u"MacTicket - Modulo de Impresion")
root.geometry('400x400+200+200')

label_1 = tk.Label(root, text=u"Ingresar ID del Ticket: ")
# shows only * when entering a password
id_entry = tk.Entry(root)#, show="*")
# cursor here
id_entry.focus_set()
# click on button to query the db
button = tk.Button(root, text=u"Consultar", command=query)
# label to show result
label_2 = tk.Label(root)

# simple widget layout, stack vertical
#label_1.pack(pady=2)
label_1.grid(row=0,column=1,columnspan=2)
id_entry.grid(row=1,column=1,columnspan=2)
#id_entry.pack(pady=5)
#button.pack(pady=5)
button.grid(row=2,column=1,columnspan=2)
#label_2.pack(pady=5)
#label_2.grid(row=3,column=1)

# start the event loop
root.mainloop()
