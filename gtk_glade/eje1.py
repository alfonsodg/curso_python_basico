import sys
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)

class Demo:

    def __init__(self):
        self.gladefile = "ejemplo_1.glade"
        self.wTree = gtk.glade.XML(self.gladefile)
        self.Cabecera = self.wTree.get_widget("Titulo")
        self.Entrada = self.wTree.get_widget("Entrada")
        dic = { "on_Boton_clicked" : self.on_boton_OK_clicked,
                "on_window1_destroy" : gtk.main_quit }
        self.wTree.signal_autoconnect(dic)

    def on_boton_OK_clicked(self,widget):
        print "EJEMPLO"
        texto=self.Entrada.get_text()
        self.Entrada.set_text("Ejemplo HJA")
        self.Cabecera.set_text(texto)
        print "EJEMPLO 22"

if __name__ == "__main__":
    hwg = Demo()
    gtk.main()

