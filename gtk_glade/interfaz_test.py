import sys
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)

class Ejemplo:

    def __init__(self):
        self.gladefile = "demo_min.glade"
        self.Conjunto = gtk.glade.XML(self.gladefile)
        dic = {
                "on_VentanaPrincipal_destroy" : gtk.main_quit }
        self.Conjunto.signal_autoconnect(dic)

if __name__ == "__main__":
    hwg = Ejemplo()
    gtk.main()
