import sys
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)
    
class Datos:
    
    def __init__(self):
        self.gladefile = "interfaz.glade"  
        self.Conjunto = gtk.glade.XML(self.gladefile)
        self.CajaTexto = self.Conjunto.get_widget("NombrePersona")
        self.CajaTitulo = self.Conjunto.get_widget("EtiquetaGlobal")

        dic = { "on_Boton_clicked" : self.on_Boton_clicked,
                "on_VentanaPrincipal_destroy" : gtk.main_quit }
        self.Conjunto.signal_autoconnect(dic)
        
    def on_Boton_clicked(self,widget):
        #print "HOLA"
        valor=self.CajaTexto.get_text()
        self.CajaTitulo.set_text(valor)
        self.CajaTexto.set_text("Gracias!!!")

if __name__ == "__main__":
    hwg = Datos()
    gtk.main()

