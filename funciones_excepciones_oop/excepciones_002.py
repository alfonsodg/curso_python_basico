try:  # intenta
    print 12 / 1
except Exception, e:  # en caso excepcion
    print 'Ha habido un error', e
else:  # de lo contrario (SI NO HA HABIDO ERROR)
    print 'Todo bien'
finally:  # finalmente (SIEMPRE)
    print 'Seguimos con el programa'
