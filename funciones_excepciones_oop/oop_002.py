class Movimiento:
    Velocidad=0
    
    def __init__(self):
        self.Velocidad=50
    
    def avanzar(self,valor):
        print 'Estas avanzando',valor,'metros'
#        print 'La velocidad inicial es',self.Velocidad
    
    def retroceder(self,valor):
        print 'Estas retrocediendo',valor,'metros'
        
    def izquierda(self,valor):
        print 'Das la vuelta en',valor,'grados a la izquierda'
        
    def derecha(self,valor):
        print 'Das la vuelta en',valor,'grados a la derecha'
    
class Aereo(Movimiento):
    
    def despegar(self,altitud):
        print 'Estoy volando',altitud,'mts sobre el nivel del mar'

class Espacio(Movimiento,Aereo):
    
    def velocidad(self,valor):
        print 'Estas en velocidad WARP',valor

Carro=Movimiento()
Carro.avanzar(30)
Carro.avanzar(10)
Carro.izquierda(45)
Carro.avanzar(100)
Carro.derecha(10)
Carro.retroceder(67)

Avion=Aereo()
Avion.avanzar(100)
Avion.despegar(1000)

Voyager=Espacio()
Voyager.despegar(4000)
Voyager.velocidad(9.5)
