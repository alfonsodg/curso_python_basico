class Futbolista:
    Velocidad=40
    Aceleracion=70
    Resistencia=30
    TonoMuscular=50
    Nacionalidad='Peruano'
    Nombre='ND'
    
    def __init__(self,nom=Nombre,vel=Velocidad,acel=Aceleracion,tono=TonoMuscular):
        self.Nombre=nom
        self.Velocidad=vel
        self.Aceleracion=acel
        self.TonoMuscular=tono
    
    def correr(self,vel):
        print 'El futbolista',self.Nombre,'corre a una velocidad de',vel
    
    def patear(self,fuerza,direccion):
        calculo=(self.Velocidad+self.Aceleracion)/self.TonoMuscular
        desar=fuerza-calculo
        print self.Nombre,'ha pateado en direccion',direccion
        print 'La velocidad desarrollada es de',desar
    
    def pase(self,fuerza,direccion,velocidad,quien):
        print self.Nombre
        print 'Le pasa la pelota a',quien
        print 'en direccion',direccion
        print 'con una fuerza de',fuerza
        print 'el balon se desplaza a una vel',velocidad



Juan=Futbolista()
#Juan.Nombre='Juan Ramirez'
#Juan.correr(10)
Juan.patear(45,'NE')
#Juan.pase(40,'NE',60,'Roberto Braulio')
Alberto=Futbolista('Alberto Piazza',80,90,100)
#Alberto.Nombre='Alberto Piazza'
#Alberto.Velocidad=80
#Alberto.TonoMuscular=100
Alberto.patear(45,'NE')
